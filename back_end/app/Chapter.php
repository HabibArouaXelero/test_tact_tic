<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    public function questions()
    {
        $this->hasMany(Question::class);
    }

    public function domaine()
    {
        $this->belongsTo(Domaine::class);
    }
}
