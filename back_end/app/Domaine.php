<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Domaine extends Model
{
    public function chapters()
    {
        $this->hasMany(Chapter::class);
    }
}
