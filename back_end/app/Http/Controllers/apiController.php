<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Chapter;
use App\Domaine;
use App\Question;
use App\Response;
class apiController extends Controller
{
    //Response bloc
    public function addResponse(Request $request)
    {
        try
        {
            $response = new Response();
            $response->content = $request->contents;
            $response->question_id = 0;
            $response->created_at = date('Y-m-d H:i:s');
            $response->updated_at = date('Y-m-d H:i:s');
            $response->save();
            return response()->json
            (
                [
                    "resposne" => "success",
                    "message" => "The insertion of response carried out with success"
                ],200
            );
        }
        catch(\Exception $e)
        {
            return response()->json
            (
                [
                    "response" => "error",
                    "message" => $e->getMessage(),
                ],200
            );
        }
    }

    public function updateResponse(Request $request)
    {
        try
        {
            $response = Response::find($request->id);
            $response->content = $request->contents;
            $response->updated_at = date('Y-m-d H:i:s');
            $response->save();
            return response()->json
            (
                [
                    "resposne" => "success",
                    "message" => "The updating of response carried out with success"
                ],500
            );
        }
        catch(\Exception $e)
        {
            return response()->json
            (
                [
                    "response" => "error",
                    "message" => $e->getMessage(),
                ],500
            );
        }
    }

    public function deleteResponse(Request $request)
    {
        try
        {
            $response = Response::find($request->id);
            $response->delete();
            return response()->json
            (
                [
                    "resposne" => "success",
                    "message" => "The deleting of response carried out with success"
                ],500
            );
        }
        catch(\Exception $e)
        {
            return response()->json
            (
                [
                    "response" => "error",
                    "message" => $e->getMessage(),
                ],500
            );
        }
    }

    public function getAllResponse()
    {
        return Response::all();
    }

    //Question bloc
    public function addQuestion(Request $request)
    {
        try
        {
            $question = new Question();
            $question->content = $request->contents;
            $question->chapter_id = 0;
            $question->updated_at = date('Y-m-d H:i:s');
            $question->save();
            return response()->json
            (
                [
                    "resposne" => "success",
                    "message" => "The insertion of question carried out with success"
                ],200
            );
        }
        catch(\Exception $e)
        {
            return response()->json
            (
                [
                    "response" => "error",
                    "message" => $e->getMessage(),
                ],200
            );
        }
    }

    public function addResponseToQuestion(Request $request)
    {
        try
        {
            $question = Question::find($request->id);
            //return $question;
            $response = new Response();
            $response->content = $request->contents;
            $response->question_id = $question->id;

            $response->created_at = date('Y-m-d H:i:s');
            $response->updated_at = date('Y-m-d H:i:s');
            $response->save();
            return response()->json
            (
                [
                    "resposne" => "success",
                    "message" => "The insertion of response in question is carried out with success"
                ],200
            );
        }
        catch(Exception $e)
        {
            return response()->json
            (
                [
                    "response" => "error",
                    "message" => $e->getMessage(),
                ],200
            );
        }
    }

    public function updateQuestion(Request $request)
    {
        try
        {
            $question = Question::find($request->id);
            $question->content = $request->contents;
            $question->created_at = date('Y-m-d H:i:s');
            $question->updated_at = date('Y-m-d H:i:s');
            $question->save();
            return response()->json
            (
                [
                    "resposne" => "success",
                    "message" => "The updating of question carried out with success"
                ],200
            );
        }
        catch(\Exception $e)
        {
            return response()->json
            (
                [
                    "response" => "error",
                    "message" => $e->getMessage(),
                ],200
            );
        }
    }

    public function deleteQuestion(Request $request)
    {
        try
        {
            $question = Question::find($request->id);
            $question->delete();
            return response()->json
            (
                [
                    "resposne" => "success",
                    "message" => "The delating of response carried out with success"
                ],500
            );
        }
        catch(\Exception $e)
        {
            return response()->json
            (
                [
                    "response" => "error",
                    "message" => $e->getMessage(),
                ],500
            );
        }
    }

    public function getAllQuestions()
    {
        return Question::all();
    }

    public function questionsDetails()
    {
        try
        {
            $questions = Question::all();
            $res = [];
            foreach ($questions as $v)
                $res[] = $this->findQuestionById($v->id);
            return response()->json($res, 200);
        }
        catch(\Exception $e)
        {
            return response()->json
            (
                [
                    "response" => "error",
                    "message" => $e->getMessage(),
                ],500
            );
        }
    }

    public function findQuestionById($id)
    {
        $question = Question::find($id);
        $res = array
        (
            "id" => $question->id,
            "content" => $question->content,
            "responses" => Response::where('question_id',$question->id)->get()
        );
        return $res;
    }

    //Chapter bloc
    public function addChapter(Request $request)
    {
        try
        {
            $chapter = new Chapter();
            $chapter->title = $request->title;
            $chapter->description = $request->description;
            $chapter->domaine_id = 0;
            $chapter->created_at = date('Y-m-d H:i:s');
            $chapter->updated_at = date('Y-m-d H:i:s');
            $chapter->save();
            return response()->json
            (
                [
                    "resposne" => "success",
                    "message" => "The insertion of chapter carried out with success"
                ],200
            );
        }
        catch(\Exception $e)
        {
            return response()->json
            (
                [
                    "response" => "error",
                    "message" => $e->getMessage(),
                ],200
            );
        }
    }

    public function addQuestionToChapter(Request $request)
    {
        try
        {
            $chapter = Chapter::find($request->id);

            $question = new Question();
            $question->content = $request->contents;
            $question->chapter_id = $chapter->id;
            $question->save();
            return response()->json
            (
                [
                    "resposne" => "success",
                    "message" => "The insertion of question in chapter is carried out with success"
                ],200
            );
        }
        catch(Exception $e)
        {
            return response()->json
            (
                [
                    "response" => "error",
                    "message" => $e->getMessage(),
                ],200
            );
        }
    }

    public function updateChapter(Request $request)
    {
        try
        {
            $chapter = Chapter::find($request->id);
            $chapter->title = $request->title;
            $chapter->description = $request->description;
            $chapter->updated_at = date('Y-m-d H:i:s');
            $chapter->save();
            return response()->json
            (
                [
                    "resposne" => "success",
                    "message" => "The insertion of chapter carried out with success"
                ],200
            );
        }
        catch(\Exception $e)
        {
            return response()->json
            (
                [
                    "response" => "error",
                    "message" => $e->getMessage(),
                ],200
            );
        }
    }

    public function deleteChapter(Request $request)
    {
        try
        {
            $chapter = Chapter::find($request->id);
            $chapter->delete();
            return response()->json
            (
                [
                    "resposne" => "success",
                    "message" => "The deleting of chapter carried out with success"
                ],200
            );
        }
        catch(\Exception $e)
        {
            return response()->json
            (
                [
                    "response" => "error",
                    "message" => $e->getMessage(),
                ],200
            );
        }
    }

    public function getAllChapters()
    {
        return Chapter::all();
    }

    //Domaine bloc
    public function addDomaine(Request $request)
    {
        try
        {
            $domaine = new Domaine();
            $domaine->label = $request->label;
            $domaine->description = $request->description;
            $domaine->created_at = date('Y-m-d H:i:s');
            $domaine->updated_at = date('Y-m-d H:i:s');
            return response()->json
            (
                [
                    "resposne" => "success",
                    "message" => "The insertion of chapter carried out with success"
                ],200
            );
        }
        catch(\Exception $e)
        {
            return response()->json
            (
                [
                    "response" => "error",
                    "message" => $e->getMessage(),
                ],500
            );
        }
    }

    public function addChapterToDomaine(Request $request)
    {
        try
        {
            $domaine = Domaine::find($request->id);

            $chapter = new Chapter();
            $chapter->title = $request->title;
            $chapter->chapter_id = $domaine->id;
            $chapter->save();
            return response()->json
            (
                [
                    "resposne" => "success",
                    "message" => "The insertion of Chapter in domaine is carried out with success"
                ],200
            );
        }
        catch(Exception $e)
        {
            return response()->json
            (
                [
                    "response" => "error",
                    "message" => $e->getMessage(),
                ],500
            );
        }
    }

    public function updateDomaine(Request $request)
    {
        try
        {
            $domaine = Domaine::find($request->id);
            $domaine->label = $request->label;
            $domaine->description = $request->description;
            $domaine->created_at = date('Y-m-d H:i:s');
            $domaine->updated_at = date('Y-m-d H:i:s');
            return response()->json
            (
                [
                    "response" => "success",
                    "message" => "The updating of chapter carried out with success"
                ],200
            );
        }
        catch(\Exception $e)
        {
            return response()->json
            (
                [
                    "response" => "error",
                    "message" => $e->getMessage(),
                ],500
            );
        }
    }

    public function deleteDomaine(Request $request)
    {
        try
        {
            $domaine = Domaine::find($request->id);
            $domaine->delete();
            return response()->json
            (
                [
                    "resposne" => "success",
                    "message" => "The updating of domain carried out with success"
                ],200
            );
        }
        catch (\Exception $e)
        {
            return response()->json
            (
                [
                    "response" => "error",
                    "message" => $e->getMessage(),
                ],200
            );
        }
    }

    public function getAllDomaines()
    {
        return Domaine::all();
    }

    public function findDomaineById($id)
    {
        $domaine = Domaine::find($id);
        $res = array
        (
            "id"=>$domaine->id,
            "label" =>$domaine->label,
            "description" => $domaine->description,
            "chapter" => Chapter::where('domaine_id',$domaine->id)->get()
        );
    }
}
