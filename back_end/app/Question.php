<?php

namespace App;

use http\Env\Request;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public function response()
    {
        $this->hasMany(Response::class);
    }

    public function chapter()
    {
        $this>$this->belongsTo(Chapter::class);
    }
}
