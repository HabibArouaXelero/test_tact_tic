<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    public function question()
    {
        $this->belongsTo(Question::class);
    }
}
