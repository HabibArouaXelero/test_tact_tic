<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Response
Route::post("/addResponse","apiController@addResponse");
Route::post("/updateResponse","apiController@updateResponse");
Route::post("/deleteResponse","apiController@deleteResponse");
Route::get("getAllResponse","apiController@getAllResponse");

//Question
Route::post('/addQuestion',"apiController@addQuestion");
Route::post("/updateQuestion", "apiController@updateQuestion");
Route::post("/deleteQuestion","apiController@deleteQuestion");
Route::get("/getAllQuestions", "apiController@getAllQuestions");
Route::post("/addResponseToQuestion", "apiController@addResponseToQuestion");
Route::get('/questionsDetails', 'apiController@questionsDetails');

//Chapter
Route::post('/addChapter',"apiController@addChapter");
Route::post("/updateChapter", "apiController@updateChapter");
Route::post("/deleteChapter","apiController@deleteChapter");
Route::get("/getAllChapters", "apiController@getAllChapters");
Route::post("/addQuestionToChapter", "apiController@addQuestionToChapter");

//Domaine
Route::post('/addDomaine',"apiController@addDomaine");
Route::post("/updateDomaine", "apiController@updateDomaine");
Route::post("/deleteDomaine","apiController@deleteDomaine");
Route::get("/getAllDomaines", "apiController@getAllDomaines");
