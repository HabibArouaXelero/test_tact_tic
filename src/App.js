import React , {Component} from "react";
import axios from "axios";

class App extends Component
{
    state =
        {
            objects : []
        }
    componentDidMount()
    {
        axios.get("http://localhost:8000/api/questionsDetails")
            .then(res=>
            {
                this.setState(
                    {
                        objects : res.data,
						responsesObject : ""
                    }
                )
            });
    }

    render()
    {
		
        const {objects}=this.state;
        const objectList=objects.map(object=>
        {
			var tifOptions = Object.keys(object.responses).map(function(id) {
				return <p>{id.content}</p>
			});
            return (
                <div>
					<ol>
						<li>{object.id}</li>
						<li>{object.content}</li>
							{tifOptions}
					</ol>
                </div>
            )
        })
        return(
            <div>
                <div>
                    <center>
                        <h1>Questions</h1>
                    </center>
                   {objectList}

                </div>
            </div>
        )
    }
}

export default App;